import React from 'react';
import './Header.css';

export default function Header({ headingText }) {
    return(
        <header className='header'>
            <h1 className='heading-primary'>{headingText}</h1>
        </header>
    );
};